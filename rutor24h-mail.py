#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2012, Boris Popov <popov.b@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 

import getopt
import sys
import rutor.Parser
import rutor.Data
import rutor.PageLoader
import rutor.Mailer

PROGRAMM = 'rutor24h-mail.py'
VERSION = '0.1.2'

SERVER_NAME = None
PORT = None
LOGIN = None
PASSWORD = None
SSL = False
FROM_ADDR = None
FROM = None
TO_ADDR = None
TO = None
SUBJECT = None

def info ():
	print PROGRAMM + ", ver." + str (VERSION)
	print """Copyright (c) 2012, Boris Popov <popov.b@gmail.com>
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions.
"""
	return

def help ():
	info ()
	print "Usage: " + PROGRAMM + """ [OPTIONS]
       -h or --help  display this usage message;

SMTP server info:
       -s            <server name>;
       -p            <port>;
       --login       <login>;
       --password    <password>;
       --ssl         using ssl.

E-mail info:
       --from-addr   <address>;
       --from        <name>;
       --to-addr     <address>;
       --to          <name>;
       --subject     <subject>.
"""
	return

try:
	opts, args = getopt.getopt (sys.argv[1:], 'hs:p:', \
			["login=", "password=", "ssl", "from-addr=", \
			"from=", "to-addr=", "to=", "subject=", "help"])

except getopt.GetoptError:
	print >> sys.stderr, "Error of command line!"
	sys.exit (1)

for arg, val in opts:

	if arg == '-h' or arg == '--help':
		help ()
		sys.exit (0)
		
	elif arg == '-s':
		SERVER_NAME = val
	
	elif arg == '-p':
		PORT = val

	elif arg == '--login':
		LOGIN = val

	elif arg == '--password':
		PASSWORD = val

	elif arg == '--ssl':
		SSL = True

	elif arg == '--from-addr':
		FROM_ADDR = val
	
	elif arg == '--from':
		FROM = val
	
	elif arg == '--to-addr':
		TO_ADDR = val
	
	elif arg == '--to':
		TO = val
	
	elif arg == '--subject':
		SUBJECT = val


if (SERVER_NAME is None) or (PORT is None) or \
		(LOGIN is None) or (PASSWORD is None):
	print >> sys.stderr, "Server, Port, Login and Password must be defined!"
	sys.exit (3);

if (FROM_ADDR is None) or (TO_ADDR is None):
	print >> sys.stderr, "From-Addr and To-Addr must be defined!"
	sys.exit (3);

try:
	htmlpage = rutor.PageLoader.getPage24h ()

except:
	print >> sys.stderr, "Error of loading page!\n"
	sys.exit (4)
	
parser = rutor.Parser.Parser (htmlpage, rutor.PageLoader.BASE_ADDRESS)
try:
	parser.parse ()

except:
	print >> sys.stderr, "Error of parsing page!\n"
	sys.exit (5)


if len (parser.Records) == 0:
	print >> sys.stdout, "Nothing to print!\n"
	sys.exit (0)

message = ''
for record in parser.Records:
	
	message += record.Description
	message += '\n'
	message += ("size: " + record.Size.Size + ' ' + record.Size.SizeOf)
	message += '\n'
	message += "torrent: " + record.Links.Torrent
	message += '\n'
	message += "description: " + record.Links.Page
	message += '\n'
	message += "------\n\n"


mailer = rutor.Mailer.Mailer (SERVER_NAME, PORT, \
		LOGIN, PASSWORD, SSL)

if FROM is None:
	if mailer.send (message, FROM_ADDR, TO_ADDR):
		sys.exit (0)

elif TO is None:
	if mailer.send (message, FROM_ADDR, TO_ADDR, FROM):
		sys.exit (0)

elif SUBJECT is None:
	if mailer.send (message, FROM_ADDR, TO_ADDR, FROM, TO):
		sys.exit (0)

else:
	if mailer.send (message, FROM_ADDR, TO_ADDR, FROM, TO, SUBJECT):
		sys.exit (0)

print >> sys.stderr, "Error of sending e-mail!"
sys.exit (6)
