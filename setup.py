from distutils.core import setup
setup (name = 'rutor24h',
		version      = '0.1.3',
		url          = 'https://bitbucket.org/popov/rutor24h',
		author       = 'Boris Popov',
		author_email = 'popov.b@gmail.com',
		license      = 'GPLv3',
		description  = 'fetcher of torrents list from rutor.org',
		packages     = ['rutor'],
		scripts      = ['rutor24h-cli.py', 'rutor24h-mail.py'],)
