#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2012, Boris Popov <popov.b@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 

import string
from HTMLParser import *
import Data

class Parser (HTMLParser):
    def __init__ (self, HtmlPage, Host):
        HTMLParser.__init__ (self)
        self.__HtmlPage = HtmlPage
        self.__Host = Host
        self.Records = []
        self.__TROPEN = False
        self.__TDOPEN = False
        self.__LINKS = []
        self.__DATA = []
        self.__TDATA = '';
        return

    def handle_starttag (self, tag, attrs):
        if tag == 'tr':
            if not attrs:
                return
                    
            atr, val = attrs[0]
            if atr == 'class':
                if val == 'gai':
                    self.__TROPEN = True

                elif val == 'tum':
                    self.__TROPEN = True

        elif tag == 'td':
            if self.__TROPEN:
                self.__TDOPEN = True
                    
        elif tag == 'a':
            if not self.__TROPEN:
                return
            
            for atr, val in attrs:
                if atr == 'href':
                    self.__LINKS.append (val)
                    return
        return

    def handle_endtag (self, tag):
        if tag == 'tr':
            if self.__TROPEN:
                size   = Data.Size   ( self.__DATA[-2] )
                links  = Data.Links  ( self.__Host + self.__LINKS[2], self.__LINKS[0], self.__LINKS[1] )
                record = Data.Record ( self.__DATA[0], self.__DATA[1], size, links )
                self.Records.append (record)
 
                self.__TROPEN = False
                self.__LINKS = []
                self.__DATA = []
                
        elif tag == 'td':
            if self.__TROPEN:
                self.__TDOPEN = False
                self.__DATA.append (self.__TDATA)
                self.__TDATA = '';
        return

    def parse (self):
        self.Records = []
        self.feed (self.__HtmlPage)
        self.close ()
        return

    def handle_data (self, data):
        if not self.__TROPEN:
            return
        
        if self.__TDATA != '':
            self.__TDATA = self.__TDATA + ' '
        import string
        self.__TDATA = self.__TDATA + string.strip (data)
        return


if __name__ == '__main__':

    from sys import argv
    from sys import exit

    if len (argv) == 1:
        import PageLoader
        HtmlPage = PageLoader.getPage24h ()
    else:
        filename = argv[1]
        try:
            fh = file (filename, 'r')
        except:
            print "File not found!"
            exit (1)

        HtmlPage = fh.read ()
        fh.close ()
        
    parser = Parser (HtmlPage, 'http://rutor.org')
    parser.parse ()
    for record in parser.Records:
        print record
