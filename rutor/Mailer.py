#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2011, Boris Popov <popov.b@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 


import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class Mailer ():
	def __init__ (self, Server, Port, Login, Password, SSL = False):
		self._Server = Server
		self._Port = Port
		self._Login = Login
		self._Password = Password
		self._SSL = SSL
		return

	def send (self, Message, FromAddr, ToAddr, From = 'rutor24h',
			To = 'Receiver', Subject = 'INFO'):

		if self._SSL:
			Smtp = smtplib.SMTP_SSL ()

		else:
			Smtp = smtplib.SMTP ()

		try:
			mymessage = self._makeMessage (Message, FromAddr, From, ToAddr, To, Subject)
			Smtp.connect (self._Server, self._Port)
			Smtp.login (self._Login, self._Password)
			Smtp.sendmail (FromAddr, ToAddr, mymessage)
			Smtp.quit ()

		except:
			return False

		return True;

	def _makeMessage (self, Message, FromAddr, From, ToAddr, To, Subject):

	      	_Message = MIMEMultipart ("alternative")
		_Message ['Subject'] = Subject
		_Message ['From'] = From + " <" + FromAddr + ">"
		_Message ['To'] = To + " <" + ToAddr + ">"
		
		part1 = MIMEText (Message, "plain", "utf-8")
		_Message.attach(part1)
	     	return _Message.as_string ()
