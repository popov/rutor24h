#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2012, 2014, Boris Popov <popov.b@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 

from urllib2 import *
import string

############################################################
BASE_ADDRESS = 'http://rutor.org'
UserAgent = { 'key'   : 'User-Agent',
              'value' : "Mozilla/5.0 (X11; Linux x86_64; \
rv:24.0) Gecko/20140610 Firefox/24.0 Iceweasel/24.6.0"     }
Cookie = { 'key'   : 'Cookie',
           'value' : ""        }
Page1Stage = ""
Page2Stage = ""
############################################################

############################################################
def getPage24h ():
    loadPage1 ();
    fillCookie ();
    loadPage2 ();
    return Page2Stage;

def loadPage1 ():
    global Page1Stage
    Page1Stage = loadPage ()
    return

def loadPage2 ():
    global Page2Stage
    Page2Stage = loadPage ()
    return

def fillCookie ():
    start_str = "document.cookie='"
    start = string.find (Page1Stage, start_str)
    if start == -1:
        #
        #TODO exception
        #
        pass
    
    temp = Page1Stage[start:]
    end = string.find (temp, ";")
    if start == -1:
        #
        #TODO exception
        #
        pass

    temp = temp[len (start_str):end]

    global Cookie
    Cookie['value'] = temp
    return

def loadPage ():

    req = Request (BASE_ADDRESS)
    req.add_header (UserAgent['key'], UserAgent['value'])
    req.add_header (Cookie['key'], Cookie['value'])
    r = urlopen (req)
    return r.read ()
############################################################

############################################################
if __name__ == '__main__':
    print getPage24h ()
############################################################
