#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2012, Boris Popov <popov.b@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 


class Links ():
    def __init__ (self, PageLink, TorrentLink, MagnetLink):
        self.Page    = PageLink
        self.Torrent = TorrentLink
        self.Magnet  = MagnetLink
        return

    def __str__ (self):
        report  = '/---------------------------------------'
        report += '\n';
        report += '|    Page link: '
        report += self.Page
        report += '\n';
        report += '| Torrent link: '
        report += self.Torrent
        report += '\n';
        report += '|  Magnet link: '
        report += self.Magnet
        report += '\n';
        report += '\---------------------------------------'
        return report


class Size ():
    def __init__ (self, SiZe, SiZeOf = ''):
        self.Size   = SiZe
        self.SizeOf = SiZeOf
        return

    def __str__ (self):
        report  = ''
        report += 'Size: '
        report += str (self.Size)
        if self.SizeOf != '':
            report += ' ';
            report += self.SizeOf
        return report


class Record ():
    def __init__ (self, Date, Description, Size, Links):
        self.Date        = Date
        self.Description = Description
        self.Size        = Size
        self.Links       = Links
        return

    def __str__ (self):
        report  = '//======================================'
        report += '\n';
        report += '||  Date: '
        report += self.Date
        report += '\n||  ';
        report += self.Description
        report += '\n||  ';
        report += str (self.Size)
        report += '\n';
        report += str (self.Links)
        report += '\n';
        report += '\\\======================================'
        return report

    
if __name__ == '__main__':
    links  = Links ('one link', 'two link', 'three link')
    size   = Size (5, 'GB')
    record = Record ('5 Sept', 'Test description', size, links)
    print record
