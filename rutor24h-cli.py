#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2012, 2014, Boris Popov <popov.b@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 

import getopt
import sys
import rutor.Parser
import rutor.Data
import rutor.PageLoader

PROGRAMM = 'rutor24h-cli.py'
VERSION = '0.1.3'

def info ():
	print PROGRAMM + ", ver." + str (VERSION)
	print """Copyright (c) 2012, 2014, Boris Popov <popov.b@gmail.com>
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions.
"""
	return

def help ():
	info ()
	print "Usage: " + PROGRAMM + """ [OPTIONS]
       -h       display this usage message.
"""
	return


try:
	opts, args = getopt.getopt (sys.argv[1:], 'h')

except getopt.GetoptError:
	print >> sys.stderr, "Error of command line!\n"
	sys.exit (1)

for arg, val in opts:

	if arg == '-h':
		help ()
		sys.exit (0)


try:
	htmlpage = rutor.PageLoader.getPage24h ()

except:
	print >> sys.stderr, "Error of loading page!\n"
	sys.exit (2)
	
parser = rutor.Parser.Parser (htmlpage, rutor.PageLoader.BASE_ADDRESS)
try:
	parser.parse ()

except:
	print >> sys.stderr, "Error of parsing page!\n"
	sys.exit (3)


if len (parser.Records) == 0:
	print >> sys.stdout, "Nothing to print!\n"
	sys.exit (0)


for record in parser.Records:
	
	print >> sys.stdout, record.Description
	print >> sys.stdout, ("size: " + record.Size.Size + ' ' + record.Size.SizeOf)
	print >> sys.stdout, "torrent: " + record.Links.Torrent
	print >> sys.stdout, "description: " + record.Links.Page
	print >> sys.stdout, "------\n"


sys.exit (0)
